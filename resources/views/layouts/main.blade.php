<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('assets/img/logo.png') }}">
    <title>Prymage</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/fullcalendar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/plugins/morris/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">


    <!-- DataTables -->
    <link href="{{ url('backend/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('backend/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    
    <link href="{{ url('backend/datatables/buttons.dataTables.css') }}" rel="stylesheet" type="text/css">



    <!--[if lt IE 9]>
		<script src="{{ url('assets/js/html5shiv.min.js') }}"></script>
		<script src="{{ url('assets/js/respond.min.js') }}"></script>
	<![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <div class="header"> <!-- Header start -->
            <div class="header-left">
                <a href="{{ url('/home') }}" class="logo">
                    <img src="{{ url('assets/img/logo.png') }}" width="122" height="74" alt="">
					
                </a>
            </div>
            <div class="page-title-box float-left">
              <h3 class="text-uppercase">Prymage</h3>
            </div>
            
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <ul class="nav user-menu float-right">
                {{--  <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell"></i> <span class="badge badge-pill bg-primary float-right">3</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">
												<img alt="John Doe" src="assets/img/user.jpg" class="img-fluid rounded-circle">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">John Doe</span> is now following you </p>
												<p class="noti-time"><span class="notification-time">4 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">T</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Tarah Shropshire</span> sent you a message.</p>
												<p class="noti-time"><span class="notification-time">6 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">L</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Misty Tison</span> like your photo.</p>
												<p class="noti-time"><span class="notification-time">8 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">G</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Rolland Webber</span> booking appoinment for meeting.</p>
												<p class="noti-time"><span class="notification-time">12 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">T</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Bernardo Galaviz</span> like your photo.</p>
												<p class="noti-time"><span class="notification-time">2 days ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="activities.html">View all Notifications</a>
                        </div>
                    </div>
                </li>  --}}
                <li class="nav-item dropdown d-none d-sm-block">
                    {{--  <a href="javascript:void(0);" id="open_msg_box" class="hasnotifications nav-link"><i class="fa fa-comment"></i> <span class="badge badge-pill bg-primary float-right">8</span></a>  --}}
                </li>
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img"><img class="rounded-circle" src="{{ url('assets/img/user.jpg') }}" width="40" alt="Admin">
							<span class="status online"></span></span>
                        <span>{{ Auth::user()->name }}</span> <span>| <small>{{ Auth::user()->role }}</small></span>
                        
                    </a>
					<div class="dropdown-menu">
						{{--  <a class="dropdown-item" href="profile.html">My Profile</a>
						<a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
						<a class="dropdown-item" href="settings.html">Settings</a>                          --}}
                        <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                        
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right"> <!-- mobile menu -->
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="profile.html">My Profile</a>
                    <a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
                    <a class="dropdown-item" href="settings.html">Settings</a>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </div>
        </div>


        <div class="sidebar" id="sidebar"> <!-- sidebar -->
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Main</li>
                    	<li>
                            <a href="{{ url('/home') }}"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a>
                        </li>
						

						<li class="submenu">
                            <a href="#"><i class="fa fa-user" aria-hidden="true"></i> <span> Visitors</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li><a href="{{ url('add-visitor') }}">Add Visitors</a></li>
                                <li><a href="{{ url('manage-visitors') }}">Manage</a></li>
                                
                                {{--  <li><a href="about-student.html">About Visiors</a></li>  --}}
                            </ul>
                        </li>

						<li class="submenu">
                            <a href="#"><i class="fa fa-user" aria-hidden="true"></i> <span> Appointment</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li><a href="{{ url('/visitors-appointment') }}">Check-In/Check-Out</a></li>
                                <li><a href="{{ url('/visitors-log') }}">Logs</a></li>
                                <li><a href="{{url('/visitors-log')}}">Generate Report</a></li>
                           
                            </ul>
                        </li>
						{{--  <li class="submenu">
                            <a href="javascript:void(0);"><i class="fa fa-share-alt" aria-hidden="true"></i> <span>Messages</span> <span class="menu-arrow"></span></a>
                            <ul style="display: none;">
                                <li class="submenu">
                                    <a href="javascript:void(0);"><span>Email</span> <span class="menu-arrow"></span></a>
                                    <ul style="display: none;">
                                        <li><a href="compose.html"><span>Compose Mail</span></a></li>
                                        <li>
                                            <a href="inbox.html"> <span> Inbox</span> </a>
										</li>
                                        <li><a href="mail-view.html"><span>Mailview</span></a></li>
                                    </ul>
                                </li>
                                <li>
									<a href="chat.html"> Chat <span class="badge badge-pill bg-primary float-right">5</span></a>
								</li>
								 <li class="submenu">
									<a href="#"><span> Calls</span> <span class="menu-arrow"></span></a>
									<ul class="list-unstyled" style="display: none;">
										<li><a href="voice-call.html">Voice Call</a></li>
										<li><a href="video-call.html">Video Call</a></li>
										<li><a href="incoming-call.html">Incoming Call</a></li>
									</ul>
								</li>
								 <li>
									<a href="contacts.html"> Contacts</a>
								</li>
                            </ul>
                        </li>
                         --}}
                        {{--  calendar was here  --}}
                       {{--  Management  --}}
                        
                       @if(Auth::user()->role == 'admin')
                       <li class="submenu">
                                <a href="#"><i class="fa fa-user" aria-hidden="true"></i> <span> Users Mangement</span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" style="display: none;">
                                    <li><a href="{{ url('/add-users') }}">Add</a></li>
                                    <li><a href="{{ url('/home') }}">Manage</a></li>
                                    {{--  <li><a href="{{ url('/home') }}">Edit Users</a></li>  --}}
                                </ul>
                        </li>
                        @endif

                        <li>
                            <a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Settings</a>
                            <ul class="list-unstyled" style="display: none;">
                                    {{--  <li><a href="{{ url('/home') }}">Change Password</a></li>
                                    <a href="{{ url('/home') }}">Logout</a>  --}}
                                    
                                    <li><a href="{{ url('change-password') }}" class="dropdown-item">Change Password</a></li>
                                    
                                    <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                    </a>
            
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    
                                    </li>

                                   
                                    
                                </ul>
                        </li>
                       {{--  UI elements - level 1 here  --}}
                    

                    </ul>
                </div>
            </div>
        </div>

        {{--  yield content  --}}
        @yield('content')

        </div>
        <div class="sidebar-overlay" data-reff=""></div>
     <script type="text/javascript" src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/plugins/morris/morris.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/plugins/raphael/raphael-min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/jquery.fullcalendar.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/chart.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/app.js') }}"></script>
    
    
    {{--  <script type="text/javascript" src="{{ url('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/dataTables.bootstrap4.min.js') }}"></script>  --}}


    <!-- Datatables-->
    <script src="{{ url('backend/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('backend/datatables/dataTables.responsive.min.js') }}"></script>

    <script src="{{ url('backend/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('backend/datatables/jszip.min.js') }}"></script>
    <script src="{{ url('backend/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ url('backend/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ url('backend/datatables/buttons.html5.min.js') }}"></script>
    <script>
       


            $(document).ready(function () {
               
                // Setup - add a text input to each footer cell
                $('#datatable1 tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                    console.log(title);
                } );
               
                
                var reportdata = $('#datatable1').DataTable({
                                    dom: 'Bfrtip',
                                    buttons: [
                                    'copyHtml5',
                                    'excelHtml5',
                                    'csvHtml5',
                                    'pdfHtml5'
                                    ],
                                
                                
                                });


                   // Apply the search
                   reportdata.columns().every( function () {
                    var that = this;
            
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );

            });
    </script>

    @yield('js')

  

</body>
</html>