<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('assets/img/logo.png') }}">
    <title>Primage</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
    <!--[if lt IE 9]>
		<script src="{{ url('assets/js/html5shiv.min.js') }}"></script>
		<script src="{{ url('assets/js/respond.min.js') }}"></script>
	<![endif]-->
</head>

<body>
     <div class="main-wrapper">
        <div class="account-page">
            
           @yield('content')


        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>
    <script type="text/javascript" src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
</body>


</html>