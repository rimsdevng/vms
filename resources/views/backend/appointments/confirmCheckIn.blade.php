@extends('layouts.main')

@section('title')
    Visitors Checking Page
@endsection 

@section('content')

<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
        <div class="page-header">
            @include('notification')
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                    <h5 class="text-uppercase">Visitors Details</h5>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="index.html">Home</a></li>
                        <li class="list-inline-item"><a href="index.html">Visitors</a></li>
                        <li class="list-inline-item"> About Visitor</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-page">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="aboutprofile-sidebar">
                        <div class="row">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                                <div class="aboutprofile">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="aboutprofile-pic">
                                                        <img class="card-img-top" src="{{ $visitor->image }}" alt="Card image">
                                                    </div>
                                                    <div class="aboutprofile-name">
                                                        <h5 class="text-center mt-2">{{ $visitor->fname }} {{ $visitor->sname }}</h5>
                                                        <p class="text-center">{{ $visitor->visitor_id }}</p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="aboutme-profile mt-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Visitors Profile:</h4>
                                            
                                        </div>
                                        <div class="card-body">
                                            
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item"><b>Gender</b><a href="#" class="float-right">{{ $visitor->gender }}</a></li>
                                                <li class="list-group-item"><b>Email</b><a href="#" class="float-right"> {{ $visitor->email }}	</a></li>
                                                <li class="list-group-item"><b>Phone number</b><a href="#" class="float-right">{{ $visitor->phone }}</a></li>
                                                <li class="list-group-item"><b>Date Created</b><a href="#" class="float-right">{{ $visitor->created_at }}</a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="aboutprofile-address mt-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Address</h4>
                                        </div>
                                        <div class="card-body">
                                            <address class="text-center">
                                               {{$visitor->address}}
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                            <!-- Nav tabs -->
                                                <ul class="nav customtab">
                                                    <li class="nav-item"><a href="#" class="nav-link active show">Kindly review before confirming check-in</a></li>
                                                </ul>
                                                                    
                                                        <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div class="tab-pane active show">
                                                        <div id="biography" class="biography">
                                                            <div class="row">
                                                               
                                                                <div class="col-md-6 col-6"> <strong>Previous Purpose</strong>
                                                                    <br>
                                                                    <p class="text-muted">{{ $visitor->purpose }}</p>
                                                                </div>
                                                                <div class="col-md-6 col-6"> <strong>Previous Host</strong>
                                                                    <br>
                                                                    <p class="text-muted">{{ $visitor->whoToSee }}</p>
                                                                </div>
                                                            </div>
                                                            <hr>
                                     
                                                            <form action="{{ url('check-in/'.$visitor->vid) }}" method="POST">
                                                                {{ csrf_field() }}
                                                                <div class="form-group row">
                                                                    <label class="col-form-label col-md-2">Who to see</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control form-control-lg" name="whoToSee" value="{{ $visitor->whoToSee }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-form-label col-md-2">Purpose</label>
                                                                    <div class="col-md-10">
                                                                        <select class="form-control form-control-lg" name="purpose" required>
                                                                            <option>Personal</option>
                                                                            <option>Official</option>
                                                                            <option>Delivery</option>
                                                                            
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ url('visitors-appointment') }}" class="btn btn-warning btn-lg pull-right">Ignore</a>
                                                               
                                                                    
                                                                <span>
                                                                <button type="submit" class="btn btn-success btn-lg pull-right mr-3">Confirm Check-In</button>
                                                                </span>
                                                                

                                                            </form>
                                                            <br>
                                                           
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection 