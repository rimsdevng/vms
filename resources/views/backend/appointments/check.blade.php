@extends('layouts.main')

@section('title')
    Visitors Checking Page
@endsection 

@section('content')


<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
    <div class="page-header">
            <div class="row">
                
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                        @include('notification')
                    <h5 class="text-uppercase">Visitors Appointment</h5>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="#">Home</a></li>
                        <li class="list-inline-item"><a href="#">Management</a></li>
                        <li class="list-inline-item"><a href="#">Appointments</a></li>
                        <li class="list-inline-item">Check-in/Check-out</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="content-page">
           <form>
            <div class="row filter-row">
                {{--  <div class="col-sm-6 col-md-3">
                    <div class="form-group custom-mt-form-group">
                        <select >
                            <option>Select buyer</option>
                            <option>Loren Gatlin</option>
                            <option>Tarah Shropshire</option>
                         </select>
                         <label class="control-label">Purchased By</label><i class="bar"></i>
                    </div>
                </div>  --}}
               
                {{--  <div class="col-sm-9 col-md-9">
                    <div class="form-group custom-mt-form-group">
                            <input class="form-control floating" onkeyup="submit()" name="fname" type="text" >
                        <label class="control-label">Search</label><i class="bar"></i>
                    </div>
                </div>  --}}
            
                {{--  <div class="col-sm-6 col-md-3">
                    <div class="form-group custom-mt-form-group">
                         <input class="form-control floating datetimepicker" type="text" >
                        <label class="control-label">To</label><i class="bar"></i>
                    </div>
                </div>  --}}
                {{--  <div class="col-sm-3 col-md-3">
                    <a href="#" class="btn btn-success btn-block mt-3 mb-2"> Search </a>
                </div>  --}}
            </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped custom-table m-b-0 datatable">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Visitor Name</th>
                                    <th>Email</th>
                                    <th>Purpose of Visit</th>
                                    <th>Phone</th>
                                    <th>Gender</th>
                                    <th class="text-center">status</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if(count($visitors)>0)

                                    <?php $count = 1; ?>
                                    @foreach($visitors as $visitor)

                                <tr>
                                    <td>
                                        <strong>  <?php echo $count;?></strong>
                                    </td>
                                    <td>{{ $visitor->sname }} {{ $visitor->fname }}</td>
                                    <td>{{ $visitor->email }}</td>
                                    <td>{{ $visitor->purpose }}</td>
                                    <td>{{ $visitor->phone }}</td>
                                    <td>{{ $visitor->gender }}</td>
                                    
                                    @if($visitor->status == 'checked_in')
                                        <td class="text-center">
                                            <div class="dropdown action-label">
                                                <a class="btn btn-white btn-sm btn-rounded" href="{{ url('check-out/'.$visitor->vid) }}" aria-expanded="false">
                                                    <i class="fa fa-dot-circle-o text-danger"></i> Check-Out
                                                </a>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown dropdown-action">
                                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{ url('/visitor-details/'.$visitor->vid) }}" title="View Details"><i class="fa fa-eye m-r-5"></i> View Details</a>
                                                    
                                                </div>
                                            </div>
                                        </td>
                                    @else 
                                        
                                    <td class="text-center">
                                            <div class="dropdown action-label">
                                                <a class="btn btn-white btn-sm btn-rounded" href="{{ url('view-check-in-info/'.$visitor->vid) }}" aria-expanded="false">
                                                    <i class="fa fa-dot-circle-o text-success"></i> Check-In
                                                </a>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="dropdown dropdown-action">
                                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{ url('/visitor-details/'.$visitor->vid) }}" title="View Details"><i class="fa fa-eye m-r-5"></i> View Details</a>
                                                    
                                                </div>
                                            </div>
                                        </td>

                                    @endif

                                </tr>
                                <?php $count ++; ?>
                          @endforeach
                      @else
          
                      <tr>
                            <td colspan="7">
                                <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no Visitors Details </h3>
                            </td>
                        </tr>
        
        
                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>

@endsection 