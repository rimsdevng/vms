@extends('layouts.main')

@section('title')
    Manage Visitor
@endsection 

@section('content')


<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
    <div class="page-header">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                    <h5 class="text-uppercase">Visitor list</h5>
                    @include('notification')

                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="{{url('/home')}}">Home</a></li>
                        <li class="list-inline-item"><a href="{{url('/manage-visitors')}}">Visitor</a></li>
                        <li class="list-inline-item"> Visitor list</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-3">
              
            </div>
            <div class="col-sm-8 col-9 text-right m-b-20">
                <a href="{{ url('add-visitor') }}" class="btn btn-primary float-right btn-rounded"><i class="fa fa-plus"></i> Add Visitor</a>
                {{--  <div class="view-icons">
                    <a href="{{ url('manage-visitors') }}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                    <a href="{{ url('manage-visitors') }}" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                </div>  --}}
            </div>
        </div>
    <div class="content-page">
            <div class="row filter-row">
                <div class="col-sm-6 col-md-3">
                    <div class="form-group custom-mt-form-group">
                        <input type="text" id="visitor_id" />
                        <label class="control-label">Visitor ID</label><i class="bar"></i>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group custom-mt-form-group">
                        <input type="date" id="date_to"/>
                        <label class="control-label">Date From</label><i class="bar"></i>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="form-group custom-mt-form-group">
                        <input type="date" id="date_from" />
                        <label class="control-label">Date To</label><i class="bar"></i>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <button id="btn-search" type="button" class="btn btn-success btn-block mt-3 mb-2"> Search </button>
                </div>
        </div> 


        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="visitors_tbl" class="table visitors-table table-striped custom-table">
                        <thead>
                            <tr>
                                {{--  <th style="width:20%;">Name </th>  --}}
                                <th>S/N</th>
                                <th style="width:20%;">Visitor Name</th>
                                <th>Gender</th>
                                {{--  <th>Address</th>  --}}
                                {{--  <th>Occupation</th>  --}}
                                <th>Email</th>
                                <th>Mobile</th>
                                <th class="text-right" style="width:15%;">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--  notification box  --}}
</div>
</div>

{{-- <div id="delete_employee" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content modal-md">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Student</h4>
                </div>
                <form>
                    <div class="modal-body card-box">
                        <p>Are you sure want to delete this?</p>
                        <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                            {{-- <a href="{{ url('delete-visitor/'.$visitor->vid) }}" class="btn btn-danger">Delete</a> --}}
                        </div>
                    </div>
                </form>
            </div>
        </div> --}}
 <div id="message-box" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <h4 class="modal-title">Alert Box</h4>
            </div>
            <form>
                <div class="modal-body card-box">
                    <p id="data-message"></p>
                    <div class="m-t-20"> 
                        <a href="#" class="btn btn-white" data-dismiss="modal"> Close</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection 

@section('js')
    <script>
        $(document).ready(function () {

            //$(".date").datepicker({
            //    todayHighlight: !0,
              //  autoclose: !0,
            //    startDate: '-0m',
            //    endDate: '+0d',
            //    clearBtn: true,
            //    showOnFocus: 'true',
            //    // format: 'dd/mm/yyyy',
            //});


            var form = $('#search-form');

            var btn_search = $('#btn-search');

            btn_search.on('click', function () {

                var visitor_id = $('#name').val();
                var date_from = $('#date_from').val();
                var date_to = $('#date_to').val();

                var URL = `{{ URL::to('/report/filter') }}`;

                if (visitor_id !== "" || (date_from !== "" && date_to !== "")){

                    var visitor = $("#visitors_tbl").DataTable({
                        dom: "lBfrtip",
                        buttons: [
                            {extend: "copy", className: "btn-sm btn-success"},
                            {extend: "csv", className: "btn-sm btn-success"},
                            {extend: "excel", className: "btn-sm btn-success"},
                            {extend: "pdf", className: "btn-sm btn-success"},
                            {extend: "print", className: "btn-sm btn-success"}
                        ],
                        responsive: !0,
                        "scrollX": true,
                        "deferRender": true,
                        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "sClass": "cell-center select-checkbox",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "5%",
                                "targets": 1,
                            },
                            {"targets": 2, "sClass": "numericCol"},
                            {"targets": 3, "sClass": "numericCol"},
                            {
                                "width": "5%",
                                "targets": 4,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "5%",
                                "targets": 5,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                        ],
                        "order": [[1, 'asc']],
                        "ajax": {
                            "url": `${URL}/${visitor_id}/${date_from}/${date_to}`,
                            "dataSrc": ""
                        },
                        "aoColumns": [
                            {
                                "mData": "vid",
                            },
                            {
                                "mData": "name",
                                "render": function (row, type, data) {
                                    return data.fname + " " + data.sname;
                                }
                            },
                            {"mData": "gender"},
                            {"mData": "email"},
                            {"mData": "phone"},
                            {
                                "mData": "action",
                                "render": function (row, type, data) {
                                   // var btn_edt = `<button data-visitor='${ JSON.stringify(data) }' class="btn btn-primary mb-1 btn-sm row-edit">Edit</button>`;
                                    var btn_del = `<button type="submit" data-visitor='${ JSON.stringify(data) }' class="btn btn-danger btn-sm mb-1 row-delete">Delete</button>`;
                                    //${ btn_edt }
                                    //the btn_edt was not found in the return btn_edt
                                                return ` ${ btn_del }`;
                                }
                            },
                        ]
                    });

                    visitor.on("click", ".row-delete", function () {
                        var clickBtn = $(this);
                        var selectVistitor =JSON.parse(clickBtn.attr("data-visitor"));

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });


                        $.ajax({
                            url : `{{ URL::to('/delete-visitor') }}/${ selectVistitor.vid }`,
                            method : 'DELETE',
                            success : function (data) {
                                var modal = $('#message-box');

                                var message = $('#data-message');

                                message.html(data.message);

                                modal.modal('show');

                                visitor.ajax.reload();

                            },
                            error: function (error) {
                                console.log(error)
                            }
                        })
                    });

                    visitor.on("click", ".row-edit", function () {
                        var clickBtn = $(this);
                        var selectVistitor =JSON.parse(clickBtn.attr("data-visitor"));

                        console.log(selectVistitor);
                    });

                }
               

            })
        })
    </script>
@endsection