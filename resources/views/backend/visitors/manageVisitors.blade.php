@extends('layouts.main')

@section('title')
    Manage Visitor
@endsection

@section('content')


    <div class="page-wrapper"> <!-- content -->
        <div class="content container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                        <h5 class="text-uppercase">Visitor list</h5>
                        @include('notification')

                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                        <ul class="list-inline breadcrumb float-right">
                            <li class="list-inline-item"><a href="index.html">Home</a></li>
                            <li class="list-inline-item"><a href="index.html">Visitor</a></li>
                            <li class="list-inline-item"> Visitor list</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-3">

                </div>
                <div class="col-sm-8 col-9 text-right m-b-20">
                    <a href="{{ url('add-visitor') }}" class="btn btn-primary float-right btn-rounded"><i class="fa fa-plus"></i> Add Visitor</a>
                    {{--  <div class="view-icons">
                        <a href="{{ url('manage-visitors') }}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                        <a href="{{ url('manage-visitors') }}" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                    </div>  --}}
                </div>
            </div>
            <div class="content-page">
                {{--  <div class="row filter-row">
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group custom-mt-form-group">
                            <input type="text"  />
                            <label class="control-label">Visitor ID</label><i class="bar"></i>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group custom-mt-form-group">
                            <input type="text"  />
                            <label class="control-label">Visitor Name</label><i class="bar"></i>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group custom-mt-form-group">
                            <input type="text"  />
                            <label class="control-label">Mobile</label><i class="bar"></i>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <a href="#" class="btn btn-success btn-block mt-3 mb-2"> Search </a>
                    </div>
                </div>  --}}


                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped custom-table datatable">
                                <thead>
                                <tr>
                                    {{--  <th style="width:20%;">Name </th>  --}}
                                    <th>S/N</th>
                                    <th style="width:20%;">Visitor Name</th>
                                    <th>Gender</th>
                                    {{--  <th>Address</th>  --}}
                                    {{--  <th>Occupation</th>  --}}
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th class="text-right" style="width:15%;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($visitors)>0)

                                    <?php $count = 1; ?>
                                    @foreach($visitors as $visitor)
                                        <tr>
                                            <td>

                                                <?php echo $count;?>

                                            </td>
                                            <td><a href="{{ url('/visitor-details/'.$visitor->vid) }}">{{ $visitor->fname }} <span>{{ $visitor->sname }}</span></a></td>
                                            <td>{{ $visitor->gender  }}</td>
                                            <td>{{  $visitor->email }}</td>
                                            <td>{{  $visitor->phone }}</td>
                                            <td class="text-right">

                                                <a href="{{ url('get-edit-visitor/'.$visitor->vid) }} " class="btn btn-primary mb-1 btn-sm">
                                                    Edit
                                                </a>
                                                <button type="submit" data-toggle="modal" data-target="#delete_employee" class="btn btn-danger btn-sm mb-1">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>

                                        <?php $count ++; ?>

                                    @endforeach


                                @else

                                    <tr>
                                        <td colspan="7">
                                            <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no Visitors Details </h3>
                                        </td>
                                    </tr>

                                @endif

                                {{$visitors->links()}}
                                {{--  other table row  --}}

                                <button class="btn btn-success pull-right" onclick="window.print();" > <i class="fa fa-print"></i>Print</button>
                                </tbody>
     
    
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{--  notification box  --}}
        </div>
    </div>
    <div id="delete_employee" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content modal-md">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Student</h4>
                </div>
                <form>
                    <div class="modal-body card-box">
                        <p>Are you sure want to delete this?</p>
                        <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                            <a href="{{ url('delete-visitor/'.$visitor->vid) }}" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection