@extends('layouts.main')

@section('content')
<br><br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8"> 
            <div class="card">
                
                <div class="card-header">{{ __('Please verify code from phone number to activate account') }}</div>

                <div class="card-body">
                    @include('notification')

                    @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif


                    <form method="POST" action="{{ url('post-verify/'.$visitor->vid) }}">
                        @csrf

                       

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Code') }}</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" required>

                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Verify') }}
                                    
                                </button>
                                <span> <a href="{{ url('manage-visitors') }}">Verify Later</a></span>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-footer">
                    <a href="#">Request New Code</a>
                    <input type="hidden" name="phone" value="{{ request()->phone }}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
