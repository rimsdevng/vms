@extends('layouts.main')

@section('title')
    Add Visitor
@endsection 

@section('content')

{{--  
name
phone 
time of arrival (checkin)
who to see
purpose
time you leave (checkout)  --}}






<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
     <div class="page-header">
            <div class="row">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                        <h5 class="text-uppercase">Add Visitor</h5>
                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                        <ul class="list-inline breadcrumb float-right">
                            <li class="list-inline-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="list-inline-item"><a href="{{ url('/') }}">Visitor</a></li>
                            <li class="list-inline-item"> Add Visitor</li>
                        </ul>
                    </div>
                </div>
        </div>
        <div class="content-page p-4">
                <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card-title">Kindly fill in the information below</div>
                                @include('notification')
                            </div>
                        </div>
                </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="{{ url('add-visitor') }}" method="POST" enctype="multipart/form-data">
                   {{ csrf_field() }}
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group custom-mt-form-group">
                                <input type="text" name="fname" value="{{ old('fname') }}" required>
                                <label class="control-label">First Name <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group custom-mt-form-group">
                                <input type="text" name="sname" value="{{ old('sname') }}"  required>
                                <label class="control-label">SurName <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group custom-mt-form-group">
                                <select name="purpose" >
                                    <option>Personal</option>
                                    <option>Official</option>
                                    <option>Delivery</option>
                                 </select>
                                 <label class="control-label">Purpose of Visit <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            
                            <div class="form-group custom-mt-form-group">
                                    <select name="gender" id="gender">
                                            <option>Male</option>
                                            <option>Female</option>
                                    </select>
                                 <label class="control-label">Gender <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group custom-mt-form-group">
                                <input type="text" name="whoToSee" value="{{ old('whoToSee') }}"  >
                                <label class="control-label">Who To See <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group custom-mt-form-group">
                                <input type="email" name="email" value="{{ old('email') }}"  >
                                <label class="control-label">Email <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group custom-mt-form-group">
                                <input type="text" name="phone" value="{{ old('phone') }}"  >
                                <label class="control-label">Phone Number <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group custom-mt-form-group">
                                <textarea name="address" id="" cols="30" rows="3"></textarea>
                                <label class="control-label">Address (Optional)</label><i class="bar"></i>
                            </div>
                        </div>
        
                        <div class="col-sm-12">
                            <div class="form-group custom-mt-form-group">
                                    <input type="file" name="image" accept="image/*" style="margin-bottom:10px;">
                                    <label class="control-label">Visitor Image</label><i class="bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center m-t-20">
                                <button class="btn btn-primary mr-2" type="submit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Cancel</button>                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    
</div>
@endsection 