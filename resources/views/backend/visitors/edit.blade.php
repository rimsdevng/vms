@extends('layouts.main')

@section('title')
    Add Visitor
@endsection 

@section('content')

<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
     <div class="page-header">
            <div class="row">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                        <h5 class="text-uppercase">Edit Visitor</h5>
                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                        <ul class="list-inline breadcrumb float-right">
                            <li class="list-inline-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="list-inline-item"><a href="{{ url('/') }}">Visitor</a></li>
                            <li class="list-inline-item"> Edit Visitor</li>
                        </ul>
                    </div>
                </div>
        </div>
        <div class="content-page p-4">
                <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card-title">Kindly fill in the information below</div>
                                @include('notification')
                            </div>
                        </div>
                </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="{{ url('edit-visitor/'.$visitor->vid) }}" method="POST" enctype="multipart/form-data">
                   {{ csrf_field() }}
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group custom-mt-form-group">
                                <input type="text" name="fname" value="{{$visitor->fname}}" required>
                                <label class="control-label">First Name <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group custom-mt-form-group">
                                <input type="text" name="sname" value="{{$visitor->sname}}"  required>
                                <label class="control-label">SurName <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            
                            <div class="form-group custom-mt-form-group">
                                    <select name="gender" id="gender">
                                            <option>Male</option>
                                            <option>Female</option>
                                    </select>
                                 <label class="control-label">Gender <span class="text-danger">*</span></label><i class="bar"></i>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group custom-mt-form-group">
                                    <input type="email" name="email" value="{{$visitor->email}}"  >
                                    <label class="control-label">Email <span class="text-danger">*</span></label><i class="bar"></i>
                                </div>
                            </div>
                        
                    </div>
                  
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group custom-mt-form-group">
                                <textarea name="address" id="" cols="30" rows="3">{{$visitor->address}}</textarea>
                                <label class="control-label">Address (Optional)</label><i class="bar"></i>
                            </div>
                        </div>
        
                        <div class="col-sm-12">
                            <div class="form-group custom-mt-form-group">
                                    <input type="file" name="image" style="margin-bottom:10px;">
                                    <label class="control-label">Visitor Image</label><i class="bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center m-t-20">
                                <button class="btn btn-primary mr-2" type="submit">Submit</button>
                               <a href="{{ url('manage-visitors') }}" class="btn btn-secondary">Cancel</a> 
                       </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    
</div>
@endsection 