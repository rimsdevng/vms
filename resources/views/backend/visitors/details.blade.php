@extends('layouts.main')

@section('title')
    Add Visitor
@endsection 

@section('content')

<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
          <div class="page-header">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                    <h5 class="text-uppercase">visitor Details</h5>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="#">Home</a></li>
                        <li class="list-inline-item"><a href="#">Pages</a></li>
                        <li class="list-inline-item"> Profile</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-box m-b-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-view">
                        <div class="profile-img-wrap">
                            <div class="profile-img">            

                                <a href="#"><img class="avatar" src="{{ $visitor->image }}" alt=""></a>
                            </div>
                        </div>
                        <div class="profile-basic">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="profile-info-left">
                                        <h3 class="user-name m-t-0">{{ $visitor->fname }} {{ $visitor->sname }}</h3>
                                        <h5 class="company-role m-t-0 m-b-0">Purpose:</h5>
                                        <small class="text-muted">{{ $visitor->purpose }}</small>
                                        <div class="staff-id"> {{ $visitor->visitor_id }}</div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <ul class="personal-info">
                                        <li>
                                            <span class="title">Phone:</span>
                                            <span class="text"><a href="#">{{ $visitor->phone }}</a></span>
                                        </li>
                                        <li>
                                            <span class="title">Email:</span>
                                            <span class="text"><a href="#">{{ $visitor->email }}</a></span>
                                        </li>
                                        <li>
                                            <span class="title">Date Created:</span>
                                            <span class="text">{{ $visitor->created_at }}</span>
                                        </li>
                                        
                                        <li>
                                            <span class="title">Gender:</span>
                                            <span class="text">{{ $visitor->gender }}</span>
                                        </li>

                                        <li>
                                            <span class="title">Address:</span>
                                            <span class="text">{{ $visitor->address }}</span>
                                        </li>
                                        

                                        <li>
                                            <span class="title">Verification Status:</span>
                                            <span class="text-success">
                                             <strong>   {{ $visitor->verification_status }}</strong>
                                            </span>
                                        </li>
                                    </ul>
                    @if($visitor->verification_status == "pending")
                                                       
                                <span>
                                    <a href="{{url('/verify/'.$visitor->vid)}}" class="btn btn-warning">
                                    Verify
                                    </a>
                                </span>
                            
                            {{--  @else  --}}
                            
                    @endif
                            
                            <a href="{{ url('manage-visitors') }}" class="btn btn-success pull-right">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
 
</div> 





@endsection 