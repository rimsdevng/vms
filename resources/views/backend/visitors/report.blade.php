@extends('layouts.main')

@section('css')
@endsection

@section('body_style')
    class="skin-megna fixed-layout"
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Report Datatable</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Report Table</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">

                    <div class="col-md-10">
                        <form method="POST" id="search-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5 class="card-title">Customer Name</h5>
                                        <select class="form-control" name="name" id="name">
                                            <option value=""> -- Select Customer Name --</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <h5 class="card-title">From Date</h5>
                                        <input class="form-control date" required="" type="date" name="created_at" id="created_at">
                                    </div>

                                    <div class="col-md-4">
                                        <h5 class="card-title">To Date</h5>
                                        <input class="form-control date" required="" name="created_at" type="date" id="created_at">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Click to search</h5>
                                <button class="btn btn-success" type="button" id="btn-search">Search</button>
                            </div>
                        </div>
                        </form>
                    </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Report Table</h4>
                            <h6 class="card-subtitle">Request Report</h6>
                            <div class="table-responsive m-t-40">
                                <table id="order_tbl" class=" table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Cargo Name</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Color</th>
                                        <th>Book Date (From)</th>
                                        <th>Book Date (To)</th>
                                        <th>Comment</th>
                                        <th>Order Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('js')
    <script>
        $(document).ready(function () {

            $(".date").datepicker({
                todayHighlight: !0,
                autoclose: !0,
                startDate: '-0m',
                endDate: '+0d',
                clearBtn: true,
                showOnFocus: 'true',
                // format: 'dd/mm/yyyy',
            });


            var form = $('#search-form');

            var btn_search = $('#btn-search');

            btn_search.on('click', function () {

                var book_from = $('#book_from').val();
                var name = $('#name').val();
                var book_to = $('#book_to').val();

                //This is my challenge
                var URL = `{{ URL::to('staffs/report/filter') }}`;

                if (name !== "" || (book_from !== "" && book_to !== "")){


                    var order_tbl = $("#order_tbl").DataTable({
                        dom: "lBfrtip",
                        buttons: [
                            {extend: "copy", className: "btn-sm btn-success"},
                            {extend: "csv", className: "btn-sm btn-success"},
                            {extend: "excel", className: "btn-sm btn-success"},
                            {extend: "pdf", className: "btn-sm btn-success"},
                            {extend: "print", className: "btn-sm btn-success"}
                        ],
                        responsive: !0,
                        "scrollX": true,
                        "deferRender": true,
                        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
                        "columnDefs": [
                            {"className": "dt-center", "targets": "_all"},
                            {
                                "targets": 0,
                                "sClass": "cell-center select-checkbox",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "5%",
                                "targets": 1,
                            },
                            {"targets": 2, "sClass": "numericCol"},
                            {"targets": 3, "sClass": "numericCol"},
                            {
                                "width": "5%",
                                "targets": 4,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "5%",
                                "targets": 4,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                "width": "5%",
                                "targets": 6,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "5%",
                                "targets": 7,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "7%",
                                "targets": 8,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                            {
                                // "width": "7%",
                                "targets": 9,
                                "sClass": "cell-center",
                                "sortable": false,
                                "searchable": false,
                                "orderable": false
                            },
                        ],
                        "order": [[1, 'asc']],
                        "ajax": {
                            "url": `${URL}/${name}/${book_from}/${book_to}`,
                            "dataSrc": ""
                        },
                        "aoColumns": [
                            {
                                "mData": null,
                            },
                            {
                                "mData": "name",
                                "render": function (data, type, staffs) {
                                    console.log(staffs)
                                    return staffs.user.name;
                                    // return `<img src="" alt="Image" style="border-radius: 30px;" class="img-rounded height-30">`
                                }
                            },
                            {
                                "mData": "name",
                                "render": function (data, type, row, meta) {

                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                    // DataTable Api
                                    var currentCell = $("#order_tbl")
                                        .DataTable()
                                        .cells({"row":meta.row, "column":meta.col})
                                        .nodes(0);

                                    $.ajax({
                                        url: `{{ URL::to('staffs/cargo/show') }}/${row.name}`,
                                        dataType: 'json',
                                        method: 'GET',
                                    }).done(function (success) { $(currentCell).html(success.name); });
                                    return null;
                                }
                            },
                            {
                                "mData": "size",
                                "render": function (data, type, users) {
                                    return `${users.size} Long`;
                                }
                            },
                            {
                                "mData": "price",
                                "render": function (data, type, users) {
                                    return `GHC ${users.price}`;
                                }
                            },
                            {
                                "mData": "color",
                            },
                            {
                                "mData": "book_from",
                                "render": function (data, type, users) {
                                    return moment(users.book_from).format("MMMM Do YYYY");
                                }
                            },
                            {
                                "mData": "book_to",
                                "render": function (data, type, users) {
                                    return moment(users.book_to).format("MMMM Do YYYY");
                                    // console.log(users)
                                }
                            },
                            {
                                "mData": "comment",
                            },
                            {
                                "mData": "created_at",
                                "render": function (data, type, users) {
                                    return moment(users.created_at).format("MMMM Do YYYY, h:mm:ss a");
                                }
                            },
                        ]
                    });
                    order_tbl.on('order.dt search.dt', function () {
                        order_tbl.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                            cell.innerHTML = i + 1;
                        });
                    }).draw();

                }


            })
        })
    </script>
@endsection

