@extends('layouts.main')

@section('title')
    Manage Users
@endsection 

@section('content')


<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
    <div class="page-header">
            <div class="row">
                
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                        @include('notification')
                    <h5 class="text-uppercase">Manage Staff</h5>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="#">Home</a></li>
                        <li class="list-inline-item"><a href="#">Staff</a></li>
                        <li class="list-inline-item">Manage</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="content-page">
           <form>
            <div class="row filter-row">
                
            </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped custom-table m-b-0 datatable">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th class="text-center">Change Role</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if(count($users)>0)

                                    <?php $count = 1; ?>
                                    @foreach($users as $user)

                                <tr>
                                    <td>
                                        <strong>  <?php echo $count;?></strong>
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role }}</td>
                                  
                        @if($user->role == 'staff')
                            <td class="text-center">
                                <div class="dropdown action-label">
                                    <a class="btn btn-white btn-sm btn-rounded" href="{{ url('make-admin/'.$user->uid) }}" aria-expanded="false">
                                        <i class="fa fa-dot-circle-o text-info"></i> Make Admin
                                    </a>
                                </div>
                            </td>
                                      
                            @else 
                                        
                            <td class="text-center">
                                <div class="dropdown action-label">
                                    <a class="btn btn-white btn-sm btn-rounded" href="{{ url('make-staff/'.$user->uid) }}" aria-expanded="false">
                                        <i class="fa fa-dot-circle-o text-warning"></i> Make Staff
                                    </a>
                                </div>
                            </td>
                            

                        @endif
                        <td>
                            <a class="btn btn-danger pull-right" href="{{ url('/delete-user/'.$user->uid) }}">Delete</a>
                        </td>
                                </tr>
                                <?php $count ++; ?>
                          @endforeach
                      @else
          
                      <tr>
                            <td colspan="7">
                                <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no user Details </h3>
                            </td>
                        </tr>
        
        
                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>

@endsection 