@extends('layouts.master')

@section('title')
    User
@endsection

@section('content')
    <h1>{{ $user-&gt;name }}</h1>
    <p>Account Status:
        @if($user->verified)
            Verified
        @else
            Not Verified
        @endif
    </p>
    @if( !$user->verified )
        <p>
          <a href="{{ route(&#x27;user-verify&#x27;) }}">Verify your account now</a>
        </p>
    @endif
@endsection
