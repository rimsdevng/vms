@extends('layouts.auth')

@section('content')
<div class="container">


<h3 class="account-title text-white">Login</h3>
<div class="account-box">
    <div class="account-wrapper">
        <div class="account-logo">
            <a href="index.html"><img src="{{ url('assets/img/logo.png') }}" alt="UsersLogin"></a>
        </div>
        <form method="POST" action="{{ route('login') }}">
        @csrf							
        <div class="form-group custom-mt-form-group">
                {{-- <input type="text" /> --}} 
                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                <label class="control-label">Username or Email</label><i class="bar"></i>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group custom-mt-form-group">
                {{-- <input type="password"  /> --}} 
                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <label class="control-label">Password</label><i class="bar"></i>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group text-center custom-mt-form-group">
                <button class="btn btn-primary btn-block account-btn" type="submit">Login</button>
            </div>
            <div class="text-center">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
            </div>
        </form>
    </div>
</div>



</div>
@endsection
