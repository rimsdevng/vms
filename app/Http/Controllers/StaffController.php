<?php

namespace App\Http\Controllers;


use App\visitor;
use App\appointment;
use App\confirmation;
use App\staff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class StaffController extends Controller
{
    
     /**
     * Create a new controller instance.
     *	Systematically ban uninvited visitors.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addVisitor(){
        return view ('backend.visitors.add');
	}
	
	
	// begining of add visitor function 
    public function postAddVisitor(Request $request){
	
		try{

			DB::beginTransaction();

				$re_phone = "/(\+233)/is";

				$from_number = $request->input('phone');
				//$from_number = "9071752006";
				if($m=preg_match_all($re_phone, $from_number, $matches)){
				}else{
					$from_number = '+233' . ltrim($from_number, '0');
				}
				// die($from_number);
				$visitorsid = 'PV19-'. Str::random(5);
		
				if($request->hasFile('image')){
					$destinatonPath = '';
					$filename = '';
			
					$file = Input::file('image');
					$destinationPath = public_path().'visitor/images/';
					die($destinationPath);
					$filename = str_random(6).'_'.$file->getClientOriginalName();
					$uploadSuccess = $file->move($destinationPath, $filename);
				}else {
					$filename = url('/img/default-profile.png');
				}
		
				$visitor 			= new visitor();
				$visitor->fname 	= $request->input('fname');
				$visitor->sname 	= $request->input('sname');
				$visitor->email 	= $request->input('email');
				$visitor->password 	= bcrypt('prymage123');
				$visitor->phone 	= $from_number;
				$visitor->image 	=  $filename;
				$visitor->gender 	= $request->input('gender');
				$visitor->address 	= $request->input('address');
				$visitor->purpose 	= $request->input('purpose');
				$visitor->whoToSee 	= $request->input('whoToSee');

				$visitor->visitor_id = $visitorsid;

				

				$visitor->save();
				
				
				$code = Str::random('5');
				$phone = $visitor->phone;
				$confirmation = new confirmation();
				$confirmation->email = $visitor->email;
				$confirmation->phone =$phone;
				$confirmation->code = $code;
				$confirmation->save();
				$msgstatus = $this->sendmessage($phone, "Welcome to Prymage! Your vericafication OTP is: " . $code);
				// later you can add the verification code here
				DB::commit();
				if($msgstatus){
					// session()->flash('success','Verification code sent');
					return redirect('/verify/'.$visitor->vid)->with('success','Visitors record added, Please verify phone number to prodeed');
				}else{
					// session()->flash('error','Error something went wrong');
					return redirect()->back()->with('error','Error something went wrong');
				}
				
				
		
            	// session()->flash('success','Visitors record added, Please verify phone number to prodeed');
            
				// return redirect('/verify');				
				// return redirect()->back();
				

			}
		

		catch (\Exception $exception){
			
			return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");

            return redirect('/verify')->with('success', 'Please verify visitor to comtinue');

		}


	}
	// end add visitors 


	// -----------------------send phone verification should come here

	// public function sendPhoneVerification( Request $request ) {

	// 	try{
	// 	$phone = $request->input('phone');

	// 	$code = self::random_str(6);
		
	// 	$confirmation = new confirmation();
	// 	$confirmation->email = $phone;
	// 	$confirmation->code = $code;
	// 	$confirmation->save();
		
	// 	//tsch
		
	// 	session()->flash('success','Verification Code Sents Successfully');
	// 	return redirect()->back();

		
	// 	}catch(\Exception $exception){
	// 		return $exception->getMessage();
    //         session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
	// 		return redirect()->back();
	// 	}

	// }

	// -------------------------------end of send phone verification

	public function sendmessage ($phone, $message){
		
		$username = 'jonahrchirika';
		$password = 'rimzy-m4p';
		$messages = array(
		array('to'=>$phone, 'body'=>$message)
		);

		$result = $this->send_message( json_encode($messages), 'http://api.bulksms.com/v1/messages', $username, $password );
		// var_dump($result);

		if ($result['http_status'] != 201) {
		die( "Error sending: " . ($result['error'] ? $result['error'] : "HTTP status ".$result['http_status']."; Response was " .$result['server_response']));
		return false;
		} else {
		session()->flash( "Response " . $result['server_response']);
		// Use json_decode($result['server_response']) to work with the response further
		return true;
		}



	}


	function send_message ( $post_body, $url, $username, $password) {
		$ch = curl_init( );
		$headers = array(
		'Content-Type:application/json',
		'Authorization:Basic '. base64_encode($username . ":" . $password)
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
		// Allow cUrl functions 20 seconds to execute
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
		// Wait 10 seconds while trying to connect
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
		$output = array();
		$output['server_response'] = curl_exec( $ch );
		$curl_info = curl_getinfo( $ch );
		$output['http_status'] = $curl_info[ 'http_code' ];
		$output['error'] = curl_error($ch);
		curl_close( $ch );
		return $output;
	  } 

   // ------------------------------confirm phone verification
	
 
   public function getVerify($vid){
	 
	// $vid
		// return view ('backend.visitors.verify');

		$visitor = visitor::findorfail($vid);

		// $visitor->phone = $phone;
		// $confirmation->code = $code;
		// $msgstatus = $this->sendmessage($phone, "Welcome to Prymage! Your vericafication OTP is: " . $code);
		// $visitor = visitor::where('verification_status', 'pending')->get()->first();
		return view('backend.visitors.verify',[
			'visitor' => $visitor
		]);
	
	}
 
 
   public function confirmPhoneVerification( Request $request, $vid ) {
	
		try{
			// $phone = $visitor->phone;
			$code = $request->input('code');

			$confirmation = confirmation::where('code', $code)->get()->last();

			if($confirmation->code == $code){
				// die('Success');
				$visitor = visitor::findorfail($vid);
				$visitor->verification_status='verfied';
				$visitor->save();

				// session()->flash('success','Verification successful');
				return redirect('/manage-visitors')->with('success' , 'Verification successful');

			}else{
				session()->flash('error','Sorry unable to verify account');
				return redirect()->back();
			}

		}catch(\Exception $exception){
			return $exception->getMessage();
			session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
			
			return redirect()->back();
		}

	}

// ------------------------------end of confirm phone verification 

	public function deleteVisitor($vid){
		
		visitor::destroy($vid);

		// if ($visitor){
		// 	session()->flash('success','Deleted successfully');
		// }else{
		// 	session()->flash('error','Sorry something went wrong');

		// }
		// return redirect()->back();
		return response()->json([
			'message' => "A Visitor was deleted successfully"
		],200);
	}

    public function getReports(){
        $visitors = visitor::all();
        return view('backend.visitors.manage', [
            'visitors' => $visitors
        ]);
    }

	public function manageVisitors(){
		$visitors = visitor::orderBy('created_at','desc')->paginate(7);
		return view('backend.visitors.manageVisitors', [
			'visitors' => $visitors
		]);
	}


	public function getEditVisitor($vid){
		$visitor = visitor::find($vid);
		return view('backend.visitors.edit',[
			'visitor' => $visitor
		]);

	}

	public function postEditVisitor(Request $request, $vid){
		try{

		
			
			
			// if ( $request->hasFile( 'image' ) ) {
			// 	$fileName = Carbon::now()->timestamp . $request->file( 'image' )->getClientOriginalName();
			// 	$request->file( 'image' )->move( 'uploads/staff', $fileName );
			// 	$fileUrl = url( 'uploads/staff/' . $fileName );
			// }
			
			
			// if($request->hasFile('image')){
			// 	$destinatonPath = '';
			// 	$filename = '';
		
			// 	$file = Input::file('image');
			// 	$destinationPath = public_path().'/images/';
			// 	$filename = str_random(6).'_'.$file->getClientOriginalName();
			// 	$uploadSuccess = $file->move($destinationPath, $filename);
			// }
			
			
			
			if($request->hasFile('image')){
				$destinatonPath = '';
				$filename = '';
		
				$file = Input::file('image');
				$destinationPath = public_path().'visitor/images/';
				$filename = str_random(6).'_'.$file->getClientOriginalName();
				$uploadSuccess = $file->move($destinationPath, $filename);
			}else {
				$filename = url('/img/default-profile.png');
			}

		    $visitor =  visitor::findorfail($vid);

			$visitor->fname 	= $request->input('fname');
			// die($visitor);
			$visitor->sname 	= $request->input('sname');
			$visitor->email 	= $request->input('email');
			$visitor->image 	= url('/publicvisitor/images').$filename;
			$visitor->gender 	= $request->input('gender');
			$visitor->address 	= $request->input('address');
			
		    $visitor->save();

			return redirect('/manage-visitors')->with('success' , 'Visitors Record Updated successfully');

	    }catch (\Exception $exception){
		    session()->flash('error', 'something went wrong');
	    }

		return redirect()->back();	
	}

	public function manageVisitorsDetails($vid){
		
		$visitor =  visitor::findorfail($vid);
		return view('backend.visitors.details',[
			'visitor' => $visitor
		]);

	}

	public function getVisitorsAppointment(){

		if (Input::has('term')){
			$term = Input::get('term');
			$visitors = visitor::where('purpose','like',"%$term%")->paginate(7);
		}else{
        }
        $visitors = visitor::orderBy('created_at','desc')->paginate(7);
	
        
        return view('backend.appointments.check',[
            'visitors' => $visitors
		  ]);
	}

	// this is to display the checkin and the checkout informmation 
	public function getVisitorsLog(){

		if (Input::has('term')){
			$term = Input::get('term');
			$visitors = visitor::where('fname','like',"%$term%")->paginate(7);
		}else{
        }
        $visitors = visitor::orderBy('created_at','desc')->paginate(7);
	
        
        return view('backend.appointments.logs',[
            'visitors' => $visitors
		  ]);

	}

	//-----------------------get checkin details page
	public function getCheckInDetails($vid){


		$visitor = visitor::findorfail($vid);
		return view('backend.appointments.confirmCheckIn',[
			'visitor' => $visitor
		]);

	}
	// --------------------------end

	public function postCheckInVisitor(Request $request, $vid){

		try{

			DB::beginTransaction();

				$visitor = visitor::findorfail($vid);
				$visitor->status='checked_in';
				$visitor->purpose = $request->input('purpose'); 
				$visitor->whoToSee = $request->input('whoToSee'); 
				$visitor->save();

				$v_fname = str_slug($visitor->fname);
       			$v_sname = str_slug($visitor->sname);
        		$fullname = strtoupper($v_fname . " " . $v_sname);

				$appointment                   = new appointment();
                $appointment->vid    		   = $visitor->vid;
                $appointment->visitor_id       = $visitor->visitor_id;
                $appointment->name             = $fullname;
                $appointment->image   		   = $visitor->image; 
                $appointment->status   		   = $visitor->status;
                $appointment->gender           = $visitor->gender;
				$appointment->phone            = $visitor->phone;
				$appointment->email            = $visitor->email;
                $appointment->whoToSee         = $visitor->whoToSee;
                $appointment->purpose		   = $visitor->purpose;
                $appointment->save();
    
			DB::commit();

			
            // session()->flash('success','Visitor Checked-In successfully');
            return redirect('visitors-appointment')->with('success','Visitor Checked-In successfully');
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }
	}

 
	// this will simply change the status of the visitor to checkout and insert records which is the time into the appointment table
	public function checkOutVisitor(Request $request, $vid){
	
		try{

			DB::beginTransaction();

				$visitor = visitor::findorfail($vid);
				$visitor->status='checked_out';
				$visitor->save();

				$v_fname = str_slug($visitor->fname);
       			$v_sname = str_slug($visitor->sname);
        		$fullname = strtoupper($v_fname . " " . $v_sname);

				$appointment                   = new appointment();
                $appointment->vid    		   = $visitor->vid;
                $appointment->visitor_id       = $visitor->visitor_id;
                $appointment->name             = $fullname;
                $appointment->image   		   = $visitor->image; 
                $appointment->status   		   = $visitor->status;
                $appointment->gender           = $visitor->gender;
				$appointment->phone            = $visitor->phone;
				$appointment->email            = $visitor->email;
                $appointment->whoToSee         = $visitor->whoToSee;
                $appointment->purpose		   = $visitor->purpose;
                $appointment->save();
    
			DB::commit();

			
            // session()->flash('success','Visitor Checked-In successfully');
            return redirect('visitors-appointment')->with('success','Visitor Checked-Out successfully');
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }

	}

	public function ReportByVisitorDate($visitor_id = null, $date_from = null, $date_to = null)
    {

		if ($visitor_id == null || $date_from  == null  || $date_to  == null){
			$visitor = visitor::all();
		} else {
			$start = $date_from . ' 00:00:00';
			$end = $date_to. ' 00:00:00';

			$visitor = visitor::whereBetween('created_at', [$start , $end])
			->where('visitor_id', $visitor_id)
			->get();
		}

        return response()->json($visitor, 200);
    }


	// testing
	private function webcam(){
		return view('videoo');
	}



	
}
