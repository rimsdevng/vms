<?php

namespace App\Http\Controllers;


use App\visitor;
use App\confirmation;
use App\appointment;
use App\staff;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;




class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // count all the staffs
        $visitors = visitor::all();
        $dailyvisitors = visitor::whereDate('created_at', Carbon::today())->count();
        $appointmentin = visitor::where('status','checked_in')->count();
        $appointmentout = visitor::where('status','checked_out')->count();
        $appointments = appointment::all();
        $staff = User::all();
        return view('home', [
            'visitors' => $visitors,
            'dailyvisitors' => $dailyvisitors,
            'appointments' => $appointments,
            'appointmentin' => $appointmentin,
            'appointmentout' => $appointmentout,
            'staff' => $staff
        ]);
        
    }


    public function getAddStaff(){
        $designation = designation::all();
        return view('backend.staff.add', [
            'designation' => $designation
        ]);
    }


    public function manageStaff(){
        
        return view('backend.staff.manage');

    }
    public function viewStaffDetails(){
        return view ('backend.staff.viewStaffDetails');
    }

    public function postAddStaff (Request $request) {
        
        try{

        DB::beginTransaction();
        
        if($request->hasFile('image')){
            $destinatonPath = '';
            $filename = '';
    
            $file = Input::file('image');
            $destinationPath = public_path().'staff/images/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
        }else {
            $filename = url('default-image.png');
        }

        $did = $request->input('did');
        $designation = designation::find($did);

		$staff = new staff();
        $staff->did = $request->input('did');
		$staff->title = $request->input('title');
		$staff->fname = $request->input('fname');
		$staff->sname = $request->input('sname');
		$staff->dob  = $request->input('dob');
		$staff->gender = $request->input('gender');
		$staff->phone = $request->input('phone');
		$staff->email = strtolower($request->input('email'));
		$staff->image =  $filename;
		$staff->region = $request->input('region');
		$staff->nationality = $request->input('nationality');
		$staff->residentialAddress = $request->input('residentialAddress');
		$staff->category = $designation->category;
		$staff->password = bcrypt("Toyotagh123");    
        $staff->save();

        $s_fname = str_slug($staff->fname);
        $s_sname = str_slug($staff->sname);

        $fullname = strtoupper($s_fname . " " . $s_sname);
        $user      = new User();
        $user->stid = $staff->stid;
        $user->name = $fullname;
        $user->email = $staff->email;
        $user->password = $staff->password;
        $user->role = 'staff';
        $user->state = 'active';
		$user->save();
        
        DB::commit();
		
            session()->flash('success','Staff Added');
            
            return redirect()->back();
            
        }catch (\Exception $exception){


            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");

            return redirect()->back();
        }


    }
    
    public function getAddUsers (){
        return view('backend.addUser');

    }
    public function postAddUsers(Request $request){
        $user      = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt("Prymage123");
        $user->role = 'staff';
        $status = $user->save();
        
        if($status){
            return redirect()->back()->with('success','User Added Successfully');
        }else{
            return redirect()->back()->with('error','Something Went Wrong!');
        }
    
    }

    public function manageAllUsers(){
        $users = User::all();
        return view ('backend.manageUsers',[
            'users' => $users
        ]);
    }


    public function makeStaff( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->role = 'staff';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Staff');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }
    
    public function makeAdmin( Request $request, $uid) {
		$user = User::findorfail($uid);
		$user->role = 'admin';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Admin');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}


	public function deleteUser(Request $request, $uid) {
		$user = User::destroy($uid);

		if ($user){
			session()->flash('success','User Account Deleted');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}



    public function changePassword() {
		return view('changePassword');
	}

	public function postChangePassword(Request $request) {
		$oldpassword = $request->input('oldpassword');
		$password = $request->input('password');
		$confirmpassword = $request->input('confirmpassword');
		$user = User::find(Auth::user()->uid);

		if(password_verify($oldpassword,$user->password) ){

			if($confirmpassword == $password){
				$user->password = bcrypt($password);
				$request->session()->flash("success","Password changed successfully");
				$user->save();
				return redirect('/home');

			}
			else{
				$request->session()->flash("error", "Both new passwords don't match. Please try again");
				return redirect('/change-password');
			}

		} else {
			$request->session()->flash("error", "Current Password is wrong. Please try again.");
			return redirect('/change-password');
		}
        return redirect('/home')->with('Success', 'Password Changed Successfully');
	}

    
	public function logout() {
		auth()->logout();
		return redirect('/');
	}
}
