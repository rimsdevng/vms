<?php

namespace App\Http\Controllers;

use App\visitor;
use App\confirmation;
use App\staff;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    //

    public function sendPhoneVerification($key, $to, $msg, $sender_id){
        //defining the parameters
        $key = "KxfZvZoJfhwnSbN5h0jkfQeoW";  // Remember to put your own API Key here
        $to = "0238855067";
        $msg = "Sending SMS has never been this fun!";
        $sender_id = "Prymage"; //11 Characters maximum
        $date_time = "2017-05-02 00:59:00";

        //encode the message
        $msg = urlencode($msg);

        //prepare your url
        $url = "https://apps.mnotify.net/smsapi?"
                    . "key=$key"
                    . "&to=$to"
                    . "&msg=$msg"
                    . "&sender_id=$sender_id"
                    . "&date_time=$date_time";
        $response = file_get_contents($url) ;
        //response contains the response from mNotify

        //NB: Remember that this is a GET request

        return $response;
    }
}
