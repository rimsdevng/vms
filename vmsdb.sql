-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 03, 2019 at 06:55 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vmsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `aid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL,
  `visitor_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whoToSee` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purpose` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`aid`, `vid`, `visitor_id`, `name`, `image`, `status`, `gender`, `phone`, `email`, `whoToSee`, `purpose`, `created_at`, `updated_at`) VALUES
(1, 9, 'PV-OtezM', 'ABDUL SHERIFF', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'male', '0230000058', 'abdul@gmail.com', 'Manager', 'Official', '2019-04-27 18:53:46', '2019-04-27 18:53:46'),
(2, 5, 'PV19-6Sf6T', 'PETER DOTSE', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'male', '0230000005', 'peterdotse@gmail.com', 'Williams', 'Personal', '2019-04-27 19:41:19', '2019-04-27 19:41:19'),
(3, 5, 'PV19-6Sf6T', 'PETER DOTSE', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'male', '0230000005', 'peterdotse@gmail.com', 'Williams', 'Personal', '2019-04-27 19:42:41', '2019-04-27 19:42:41'),
(4, 10, 'PV19-6Sf6W', 'SAMUEL YEYE', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'male', '0238855066', 'samuelye@gmail.com', 'HOD', 'Official', '2019-05-03 21:12:08', '2019-05-03 21:12:08'),
(5, 35, 'PV19-2TsvL', 'ESTHER ISHAKU', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'female', '+233556321758', 'estherishaku@gmail.com', 'Williams', 'Personal', '2019-05-10 20:10:10', '2019-05-10 20:10:10'),
(6, 35, 'PV19-2TsvL', 'ESTHER ISHAKU', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'female', '+233556321758', 'estherishaku@gmail.com', 'Williams', 'Personal', '2019-05-10 20:13:45', '2019-05-10 20:13:45'),
(7, 35, 'PV19-2TsvL', 'ESTHER ISHAKU', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'female', '+233556321758', 'estherishaku@gmail.com', 'Williams', 'Personal', '2019-05-10 20:15:24', '2019-05-10 20:15:24'),
(8, 9, 'PV-OtezM', 'ABDUL SHERIFF', 'http://localhost:8000/img/default-profile.png', 'checked_out', 'male', '0230000058', 'abdul@gmail.com', 'Manager', 'Official', '2019-05-13 15:20:56', '2019-05-13 15:20:56'),
(9, 35, 'PV19-2TsvL', 'ESTHER ISHAKU', 'http://localhost:8000/img/default-profile.png', 'checked_out', 'female', '+233556321758', 'estherishaku@gmail.com', 'Williams', 'Personal', '2019-05-13 15:55:34', '2019-05-13 15:55:34'),
(10, 36, 'PV19-uAXoG', 'NASIRU IBRAHIM', 'http://localhost:8000/img/default-profile.png', 'checked_in', 'male', '+233279883067', 'nasiruibrahim@gmail.com', 'Director', 'Personal', '2019-05-13 17:03:41', '2019-05-13 17:03:41'),
(11, 36, 'PV19-uAXoG', 'NASIRU IBRAHIM', 'http://localhost:8000/img/default-profile.png', 'checked_out', 'male', '+233279883067', 'nasiruibrahim@gmail.com', 'Director', 'Personal', '2019-05-13 17:03:56', '2019-05-13 17:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `confirmations`
--

DROP TABLE IF EXISTS `confirmations`;
CREATE TABLE IF NOT EXISTS `confirmations` (
  `confid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`confid`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `confirmations`
--

INSERT INTO `confirmations` (`confid`, `phone`, `email`, `code`, `created_at`, `updated_at`) VALUES
(1, NULL, '0238855066', 's42dr', '2019-04-15 17:23:15', '2019-04-15 17:23:15'),
(2, NULL, 'ghghg', 'pbBmr', '2019-04-16 11:01:40', '2019-04-16 11:01:40'),
(3, NULL, 'sdsdsd', 'VpGAk', '2019-04-16 11:02:52', '2019-04-16 11:02:52'),
(4, NULL, '0230000005', 'cf3D6', '2019-04-17 14:42:57', '2019-04-17 14:42:57'),
(5, NULL, '0238855067', '3WMxL', '2019-04-17 17:05:35', '2019-04-17 17:05:35'),
(6, NULL, '0550005053', '46Fpp', '2019-04-17 17:08:26', '2019-04-17 17:08:26'),
(7, NULL, '08100000555', 'WLZ5c', '2019-04-21 22:08:00', '2019-04-21 22:08:00'),
(35, '+233279883067', 'nasiruibrahim@gmail.com', 'qUzR3', '2019-05-13 13:42:48', '2019-05-13 13:42:48'),
(34, NULL, '+233556321758', '8DITh', '2019-05-10 19:49:37', '2019-05-10 19:49:37'),
(33, NULL, '+233238855067', 'JrCTk', '2019-05-05 07:32:05', '2019-05-05 07:32:05'),
(37, '+233243560053', 'seth32@gmail.com', '2vBMh', '2019-05-13 19:41:44', '2019-05-13 19:41:44');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

DROP TABLE IF EXISTS `designations`;
CREATE TABLE IF NOT EXISTS `designations` (
  `did` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`did`),
  UNIQUE KEY `designations_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_04_12_120315_visitorsinfor', 1),
(7, '2019_04_15_194337_add_to_visitors_table', 2),
(8, '2019_04_16_121556_add_to_visitors_table', 3),
(9, '2019_04_18_160118_staff', 4),
(10, '2019_04_21_205811_add_to_visitors_table', 5),
(11, '2019_04_21_211553_add_to_visitors_table', 6),
(12, '2019_04_21_215655_add_to_visitors_table', 7),
(13, '2019_04_22_022943_visitorslog', 8),
(14, '2019_05_13_114955_add_to_visitors_table', 9),
(15, '2019_05_13_180212_add_to_users_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `notid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `rlid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rlid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `stid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `staffid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `did` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'male',
  `residentialAddress` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`stid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('admin','staff') COLLATE utf8mb4_unicode_ci DEFAULT 'staff',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `email`, `password`, `phone`, `code`, `active`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(2, 'jonah rimamchirika', 'jonahrchirika@gmail.com', '$2y$10$.hzzwBgt9f1JuMjjJsSGOeTsUjxuuSQRijuRCCpX4T0unXCDUh7zK', NULL, NULL, 0, NULL, '2019-04-15 11:39:01', '2019-04-15 11:39:01', 'staff'),
(3, 'williams aryee', 'williamsa@gmail.com', '$2y$10$w5R9/l1oimSpnR8JrBQAaO9ChTUwo3tN7WkC.Rq4PXf4iiQxSSFNm', NULL, NULL, 0, NULL, '2019-04-15 14:21:55', '2019-05-13 18:13:55', 'admin'),
(4, 'henry james', 'henryjames@gmail.com', '$2y$10$mDLHOXF62EOgUctxoc6HUuDi5XKO/M/limFOLqlLR00ymDrRKiSni', NULL, NULL, 0, NULL, '2019-04-15 15:57:23', '2019-05-13 18:13:19', 'admin'),
(6, 'Felix Ado', 'felixado@prymage.com', '$2y$10$zHS.CRjbDJ.RYZBd1IM2NOUKGFzCYwLds73wDTFTMN6m8x/PJrEAG', NULL, NULL, 0, NULL, '2019-05-13 17:26:59', '2019-05-13 17:26:59', 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE IF NOT EXISTS `visitors` (
  `vid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','gender') COLLATE utf8mb4_unicode_ci DEFAULT 'gender',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `whoToSee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visitor_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('checked_in','checked_out') COLLATE utf8mb4_unicode_ci DEFAULT 'checked_out',
  `purpose` enum('Official','Delivery','Personal') COLLATE utf8mb4_unicode_ci DEFAULT 'Personal',
  `address` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_status` enum('verfied','pending') COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  PRIMARY KEY (`vid`),
  UNIQUE KEY `visitors_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`vid`, `fname`, `sname`, `email`, `password`, `phone`, `code`, `image`, `gender`, `created_at`, `updated_at`, `whoToSee`, `visitor_id`, `status`, `purpose`, `address`, `verification_status`) VALUES
(5, 'Peter', 'Dotse', 'peterdotse@gmail.com', '$2y$10$PDAi0Aq8dEeCcibmGx5Os.RtT.ievm9dXTZo4VlRwOM1qEjrvSmMS', '0230000005', NULL, 'http://localhost:8000/img/default-profile.png', 'male', '2019-04-17 14:42:57', '2019-04-27 19:41:19', 'Williams', 'PV19-6Sf6T', 'checked_in', 'Personal', NULL, 'pending');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
