<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout','HomeController@logout');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/add-visitor', 'StaffController@addVisitor');
Route::post('/add-visitor', 'StaffController@postAddVisitor');

Route::get('/manage-visitors', 'StaffController@manageVisitors');
Route::get('/get-report','StaffController@getReports');

Route::get('/visitor-details/{vid}','StaffController@manageVisitorsDetails');
Route::delete('delete-visitor/{vid}','StaffController@deleteVisitor');
Route::get('get-edit-visitor/{vid}','StaffController@getEditVisitor');

Route::post('edit-visitor/{vid}','StaffController@postEditVisitor');


Route::get('/verify/{vid}', 'StaffController@getVerify');
Route::post('/post-verify/{vid}', 'StaffController@confirmPhoneVerification');
// Route::post('/verify', 'VerifyController@postVerify')->name('verify');
Route::get('/web-cam', 'StaffController@webcam');



Route::get('view-check-in-info/{vid}','StaffController@getCheckInDetails');
Route::post('check-in/{vid}','StaffController@postCheckInVisitor');
Route::get('check-out/{vid}','StaffController@checkOutVisitor');

Route::get('visitors-appointment','StaffController@getVisitorsAppointment');


Route::get('/change-password', 'HomeController@changePassword');
Route::post('/change-password','HomeController@postChangePassword');

Route::get('/add-users','HomeController@getAddUsers');
Route::post('post-add-users', 'HomeController@postAddUsers');

Route::get('manage-all-users', 'HomeController@manageAllUsers');
Route::get('make-staff/{uid}', 'HomeController@makeStaff');
Route::get('make-admin/{uid}','HomeController@makeAdmin');
Route::get('delete-user/{uid}','HomeController@deleteUser');

Route::get('visitors-log','StaffController@getVisitorsLog');

// The report manipulation
Route::get('/report/filter/{visitor_id?}/{date_from?}/{date_to?}', 'StaffController@ReportByVisitorDate')
->name('filter.visitor');


































// Route::get('verify-phone','StaffController@sendPhoneVerification');



//store check in and check out information on the log table. And possibly add more features like alerting security in case there is any issue
//reports, search, staff management, verification, 

// ===============================old 

Route::get(
    '/user',
    ['as' => 'user-index',
     'middleware' => 'auth',
     'uses' => 'UserController@show']
);

Route::get(
    '/user/new', ['as' => 'user-new', function() {
        return response()->view('newUser');
    }]
);

Route::post('/user/create', ['uses' => 'UserController@createNewUser', 'as' => 'user-create', ]
);

Route::get('/user/verify', ['as' => 'user-show-verify', function() {
        return response()->view('verifyUser');
    }]
);

Route::post('/user/verify', ['uses' => 'UserController@verify', 'as' => 'user-verify', ]
);

Route::post( '/user/verify/resend', ['uses' => 'UserController@verifyResend',
     'middleware' => 'auth',
     'as' => 'user-verify-resend']
);