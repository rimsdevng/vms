<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Visitorsinfor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        // The number of times this guys has checked in to the company.]
        //A  small  potal that will 

        Schema::create('visitors', function (Blueprint $table) {
        
            $table->increments('vid');
            $table->string('fname');
            $table->string('sname');
            $table->string('email',191)->unique();
            $table->string('password', 60);
            $table->string('phone');
            $table->string('image')->nullable();
            $table->enum('gender',['male','female','gender'])->default('gender');
            $table->string('visitor_id')->nullable();
            $table->string('purpose')->nullable();
            $table->string('whoToSee')->nullable();
            $table->enum( 'status', [ 'checked_in', 'checked_out' ])->default('checked_out');
            $table->enum( 'purpose', [ 'official','delivery', 'personal' ])->default('personal')->nullable();
            $table->string('address', 2000);          
            $table->timestamps();
        });

        Schema::create('confirmations', function(Blueprint $table){
		    $table->increments('confid');
		    $table->string('phone')->nullable();
		    $table->string('email')->nullable();
		    $table->string('code');
		    $table->timestamps();
        });
        
        Schema::create( 'staff', function ( Blueprint $table ) {
		    $table->increments( 'sid' );
		    $table->enum( 'role', [ 'host', 'staff' ] );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'password', 60 );
		    $table->string( 'phone',191 )->unique();
		    $table->string('address',1000);
		    $table->integer( 'uid' );
		    $table->enum( 'maritalStatus', [ 'Single', 'Married', 'Divorced', 'Others' ] );
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'dob' );
		    $table->rememberToken();
		    $table->string( 'image', 2000 );
		    $table->softDeletes();
		    $table->timestamps();
        } );
        

        Schema::create('notifications', function(Blueprint $table){
    	    $table->increments('notid');
    	    $table->integer('uid');
    	    $table->string('message');
    	    $table->timestamps();
        });
        

        Schema::create( 'roles', function ( Blueprint $table ) {
			$table->increments( 'rlid' );
			$table->string('name');
			$table->softDeletes();
			$table->timestamps();

        } );
        
       
        Schema::create( 'designations', function ( Blueprint $table ) {
			$table->increments('did');
			$table->string('category');
			$table->string('name',191 )->unique();
			$table->softDeletes();
			$table->timestamps();

		} );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //command to a
        
        Schema::dropIfExists('visitors');
        Schema::dropIfExists('staff');
        Schema::dropIfExists('confirmations');
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('designations');
        
        
    }
}
